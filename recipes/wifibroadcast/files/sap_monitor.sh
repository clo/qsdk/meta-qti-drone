#Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
#SPDX-License-Identifier: BSD-3-Clause-Clear

#!/bin/sh

KVER=`uname -r`

insmod /lib/modules/${KVER}/extra/qca_cnss2.ko

sleep 2

insmod /lib/modules/${KVER}/extra/mem_manager.ko

insmod /lib/modules/${KVER}/extra/qdf.ko

insmod /lib/modules/${KVER}/extra/umac.ko

insmod /lib/modules/${KVER}/extra/qca_spectral.ko

if [ ! -f /tmp/cold_boot_done ]; then
	cnsscli_w -i pci0 --enable_cold_boot_support 1
	cnsscli_w -i pci1 --enable_cold_boot_support 1

	insmod /lib/modules/${KVER}/extra/qca_ol.ko testmode=7
	insmod /lib/modules/${KVER}/extra/wifi_3_0.ko

	echo "Waiting for cold boot calibration"
	i=0
	while [ $i -lt 30 ];do
		done=`dmesg |grep "Coldboot Calibration completed successfully"`
		if [ -n "$done" ];then
			echo "done"
			touch /tmp/cold_boot_done
			break;
		fi

		echo "."
		sleep 1
		i=$(($i+1))
	done

	rmmod wifi_3_0
	rmmod qca_ol
	sleep 5
fi

insmod /lib/modules/${KVER}/extra/qca_ol.ko

insmod /lib/modules/${KVER}/extra/wifi_3_0.ko

sleep 1

cfg80211tool wifi0 enable_ol_stats 1

wlanconfig ath0 create wlandev wifi0 wlanmode specialvap -cfg80211

iw phy phy0 interface add ath0 type __ap

#cfg80211tool ath0 mcast_rate_control 1

cfg80211tool ath0 mode 11AHE20

cfg80211tool ath0 channel 161 2

cfg80211tool ath0 ssid Openwrt_specialvap

cfg80211tool ath0 hide_ssid 0

cfg80211tool ath0 wds 0

cfg80211tool ath0 backhaul 0

cfg80211tool ath0 en_6g_sec_comp 1

cfg80211tool ath0 uapsd 1

cfg80211tool ath0 stafwd 0

#cfg80211tool ath0 mscs 0

cfg80211tool ath0 ap_bridge 1

iwpriv ath0 mcast_rate 103200
iwpriv ath0 bcast_rate 103200

#iwpriv ath0 qdf_cv_lvl 0x68000a

ifconfig ath0 up

