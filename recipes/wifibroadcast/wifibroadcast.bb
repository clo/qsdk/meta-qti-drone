inherit cmake

SUMMARY = "wifibroadcast"
DESCRIPTION = "wifibroadcast based on raw WiFi radio."
HOMEPAGE = "https://github.com/OpenHD/wifibroadcast"
SECTION = "WiFi"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=84dcc94da3adb52b53ae4fa38fe49e5d"

PACKAGE_ARCH = "${MACHINE_ARCH}"

DEPENDS += " ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'libpcap', 'libpcap-ubuntu', d)} \
             libsodium \
             ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'spdlog', 'spdlog-ubuntu', d)} "

FILESPATH =+ "${WORKSPACE}/vendor/qcom/opensource/drone/:"
SRC_URI = "file://wifibroadcast/"
SRC_URI += "file://sap_monitor.sh"
S = "${WORKDIR}/wifibroadcast"

FILES_${PN} += "/usr/sbin/*"
FILES_${PN} += "/data/misc/wifi/*"
FILES_${PN} += "/usr/local/share/openhd/interface/*"

do_install(){
    install -m 0755 -d ${D}/usr/sbin
    install -m 0755 -d ${D}/data/misc/wifi
    install -m 0755 -d ${D}/usr/local/share/openhd/interface
    install -m 0755 ${S}/../build/wfb_tx ${D}/usr/sbin/wfb_tx
    install -m 0755 ${S}/../build/wfb_rx ${D}/usr/sbin/wfb_rx
    install -m 0755 ${S}/../build/wfb_keygen ${D}/usr/sbin/wfb_keygen
    install -m 0755 ${S}/../sap_monitor.sh ${D}/data/misc/wifi/sap_monitor.sh
}

