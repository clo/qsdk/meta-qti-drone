SUMMARY = "QTI Drone opensource package groups"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

LICENSE = "BSD-3-Clause-Clear"

PROVIDES = "${PACKAGES}"

PACKAGES = "packagegroup-qti-drone"

RDEPENDS_packagegroup-qti-drone = " \
        wifibroadcast \
        openhd \
        "
