inherit cmake pkgconfig
#inherit ${@bb.utils.contains('TARGET_KERNEL_ARCH', 'aarch64', 'qtikernel-arch', '', d)}
#inherit cmake

SUMMARY = "OpenHD for Drone"
DESCRIPTION = "High-definition video, 2-way UAV telemetry, and RC control signals can all be sent over a single transmission channel."
HOMEPAGE = "https://github.com/OpenHD/OpenHD"
SECTION = "Drone"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://../LICENSE;md5=1ebbd3e34237af26da5dc08a4e440464"

PACKAGE_ARCH = "${MACHINE_ARCH}"

DEPENDS += " ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'libpcap', 'libpcap-ubuntu', d)} \
             libsodium \
             ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'spdlog', 'spdlog-ubuntu', d)} \
             ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'boost', 'boost-ubuntu', d)} \
             gstreamer1.0 \
             ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'gstreamer1.0-plugins-base', 'gstreamer1.0-plugins-base-oss', d)} \
             ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'gstreamer1.0-plugins-bad', 'gstreamer1.0-plugins-bad-oss', d)} \
             gstreamer1.0-plugins-ugly \
             ${@bb.utils.contains('BASEMACHINE', 'qcs40x', 'libv4l', 'libv4l-dev', d)} \
             jpeg \
             libusb1 \
            "

FILESPATH =+ "${WORKSPACE}/vendor/qcom/opensource/drone/:"
SRC_URI = "file://OpenHD/ \
           file://wifibroadcast/ \
           file://manual_cards.json \
           file://hardware.config \
           https://github.com/OpenHD/mavlink-headers/archive/c00c83a78c11b7ba75ef17fe758ba873ef259064.zip;subdir=git;name=mavlink \
           https://github.com/nlohmann/json/archive/b2306145e1789368e6f261680e8dc007e91cc986.zip;subdir=git;name=json \
           https://github.com/gabime/spdlog/archive/927cc29444a294d76e83dfb898e797dc431ce094.zip;subdir=git;name=spdlog \
           "

SRC_URI[mavlink.md5sum] = "bf4db6eb180276a3be7a873cd85f9a59"
SRC_URI[json.md5sum] = "5b92ef9d75793992f57d796d74188258"
SRC_URI[spdlog.md5sum] = "0055a4ce46cc7cccd9ad1f25e480bc7c"

S = "${WORKDIR}/OpenHD/OpenHD/"

FILES_${PN} += "/usr/sbin/*"
FILES_${PN} += "/usr/local/share/openhd/interface/*"
FILES_${PN} += "/boot/openhd/*"

INSANE_SKIP_${PN} = "file-rdeps"

do_patch(){
    cd ${WORKDIR}
    cp -r wifibroadcast/* OpenHD/OpenHD/lib/wifibroadcast
    cp -r git/mavlink-headers-c00c83a78c11b7ba75ef17fe758ba873ef259064/* OpenHD/OpenHD/lib/mavlink-headers
    cp -r git/json-b2306145e1789368e6f261680e8dc007e91cc986/* OpenHD/OpenHD/ohd_common/lib/json
    cp -r git/spdlog-927cc29444a294d76e83dfb898e797dc431ce094/* OpenHD/OpenHD/ohd_common/lib/spdlog
}

do_install(){
    install -m 0755 -d ${D}/usr/sbin
    install -m 0777 -d ${D}/usr/local/share/openhd/interface
    install -m 0777 -d ${D}/boot/openhd
    install -m 0755 ${WORKDIR}/build/openhd ${D}/usr/sbin/openhd
    install -m 0666 ${WORKDIR}/manual_cards.json ${D}/usr/local/share/openhd/interface/manual_cars.json
    install -m 0666 ${WORKDIR}/hardware.config ${D}/boot/openhd/hardware.config
}


